#!/bin/sh

echo "basic lmde2 customisations first..."
wget https://framagit.org/stinpriza/lmde2-priza/raw/master/lmde2.sh
sh lmde2.sh

HEIGHT=15
WIDTH=40
CHOICE_HEIGHT=4
BACKTITLE="Stin Priza LMDE2 extras installation"

TITLE="M$ Corefonts"
MENU="Do you want to install M$ core fonts ?"

OPTIONS=(1 "No"
         2 "Yes")

CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE in
        1)
            echo "Proceeding.."
            ;;
        2)
	echo "installing ms core fonts..."
	sudo apt-get install -y ttf-mscorefonts-installer
            ;;
esac

TITLE="Calibri fonts"
MENU="Do you want to install Calibri fonts ?"

OPTIONS=(1 "No"
         2 "Yes")

CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE in
        1)
            echo "Proceeding.."
            ;;
        2)
	echo 'installing calibri fonts...'
	mkdir ~/.fonts
	wget https://framagit.org/stinpriza/lmde2-priza/raw/master/vistafonts-installer
	chmod +x vistafonts-installer
	sh ./vistafonts-installer
	rm -f vistafonts-installer
            ;;
esac

TITLE="Skype"
MENU="Do you want to install Skype ?"

OPTIONS=(1 "No"
         2 "Yes")

CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE in
        1)
            echo "Proceeding.."
            ;;
        2)
	echo "installing skype..."
	wget -O skype-install.deb https://repo.skype.com/latest/skypeforlinux-64.deb
	sudo dpkg -i skype-install.deb
	sudo apt-get -qq -f install -y
            ;;
esac

TITLE="Telegram Messenger"
MENU="Do you want to install Telegram messenger ?"

OPTIONS=(1 "No"
         2 "Yes")

CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE in
        1)
            echo "Proceeding.."
            ;;
        2)
	echo 'installing telegram messenger...'
	wget -O- https://telegram.org/dl/desktop/linux | sudo tar xJ -C /opt/
	sudo ln -s /opt/Telegram/Telegram /usr/local/bin/telegram-desktop
	echo 'run telegram-desktop , from command line for the 1st program start'
            ;;
esac

TITLE="Signal Messenger"
MENU="Do you want to install Signal messenger ?"

OPTIONS=(1 "No"
         2 "Yes")

CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE in
        1)
            echo "Proceeding.."
            ;;
        2)
	echo 'installing signal messenger...'
	curl -s https://updates.signal.org/desktop/apt/keys.asc | sudo apt-key add -
	echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list
	sudo apt-get update
	sudo apt-get install signal-desktop -y
            ;;
esac

# package cleanup
echo 'cleaning system...'
sudo apt-get purge -y -qq `deborphan`
sudo apt-get autoremove -y -qq
sudo apt-get clean -qq

echo 'all done, reboot now!'

exit 0
