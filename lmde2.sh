#!/bin/sh

MYUSER=`whoami`
    if [ "${MYUSER}" = "root" ]; then
        echo "[X] This script should not be executed as root!! run as user without sudo!!"
    fi

# keyboard layout & screensaver fixes
echo 'keyboard layout & screensaver settings...'
gsettings set org.mate.peripherals-keyboard-xkb.kbd layouts "['us', 'gr']"
gsettings set org.mate.peripherals-keyboard-xkb.kbd options "['terminate\tterminate:ctrl_alt_bksp', 'currencysign\teurosign:e', 'grp\tgrp:alt_shift_toggle', 'grp_led\tgrp_led:scroll']"
gsettings set org.mate.screensaver lock-enabled 'false'
gsettings set org.mate.session idle-delay '60'

# package sources
echo 'updating package sources...'
sudo wget https://framagit.org/stinpriza/lmde2-priza/raw/master/official-package-repositories.list
sudo cp official-package-repositories.list /etc/apt/sources.list.d/official-package-repositories.list

#system upgrades
sudo apt-get update -y
echo "repositories updated! removing unecessary packages, installing new ones, and upgrading system now..."
# remove unnecessary packages
sudo apt-get purge -y speech-dispatcher brltty virtualbox-* hexchat hexchat-common bluetooth bluez-cups bluez-obexd libgnome-bluetooth13 bluez-firmware gnome-bluetooth blueberry pulseaudio-module-bluetooth gir1.2-gnomebluetooth-1.0 espeak libespeak1 espeak-data
# remove asian fonts
sudo apt-get purge -y fonts-deva-extra fonts-gubbi fonts-gujr-extra fonts-guru-extra fonts-lohit-beng-assamese fonts-lohit-beng-bengali fonts-lohit-deva fonts-lohit-gujr fonts-lohit-guru fonts-lohit-knda fonts-lohit-mlym fonts-lohit-orya fonts-lohit-taml fonts-lohit-taml-classical fonts-lohit-telu fonts-nanum fonts-orya-extra fonts-pagul fonts-smc fonts-telu-extra fonts-tlwg-garuda fonts-tlwg-kinnari fonts-tlwg-laksaman fonts-tlwg-loma fonts-tlwg-mono fonts-tlwg-norasi fonts-tlwg-typist fonts-tlwg-typo fonts-tlwg-umpush fonts-tlwg-waree fonts-wqy-microhei 
# remove dmo packages
rmfiles=$(dpkg -l | grep dmo | awk '{print $2}')
sudo dpkg -P --force-depends $rmfiles
sudo apt-get update
sudo apt-get -f install -y
# update mintupdate first
sudo apt-get -y install mintupdate
sudo apt-get update
# packages install
sudo apt-get install -y dialog myspell-el-gr chromium psensor ntfs-3g libreoffice-l10n-el console-terminus fonts-droid fonts-inconsolata xfonts-terminus task-greek-desktop libreoffice-help-el deborphan bash-completion vlc-plugin-vlsub pidgin-otr pidgin-plugin-pack xul-ext-ublock-origin
# system upgrades
sudo apt-get -y upgrade
sudo apt-get -y dist-upgrade
echo 'system upgraded! continuing...'

# fix mint-update
echo 'adjusting update manager to check for new packages 10mins after boot and then weekly...'
gsettings set com.linuxmint.updates autorefresh-days 7
gsettings set com.linuxmint.updates autorefresh-hours 0
gsettings set com.linuxmint.updates autorefresh-minutes 0
gsettings set com.linuxmint.updates refresh-days 0
gsettings set com.linuxmint.updates refresh-hours 0
gsettings set com.linuxmint.updates refresh-minutes 10
gsettings set com.linuxmint.updates kernel-updates-are-safe false
gsettings set com.linuxmint.updates security-updates-are-safe true
gsettings set com.linuxmint.updates kernel-updates-are-visible true
gsettings set com.linuxmint.updates security-updates-are-visible true
gsettings set com.linuxmint.updates window-width 790
gsettings set com.linuxmint.updates window-height 693
gsettings set com.linuxmint.updates window-pane-position 344
gsettings set com.linuxmint.updates level1-is-safe true
gsettings set com.linuxmint.updates level1-is-visible true
gsettings set com.linuxmint.updates level2-is-safe true
gsettings set com.linuxmint.updates level2-is-visible true
gsettings set com.linuxmint.updates level3-is-safe false
gsettings set com.linuxmint.updates level3-is-visible true
gsettings set com.linuxmint.updates level4-is-safe false
gsettings set com.linuxmint.updates level4-is-visible true
gsettings set com.linuxmint.updates level5-is-safe false
gsettings set com.linuxmint.updates level5-is-visible false
gsettings set com.linuxmint.updates show-policy-configuration false

# weather
echo 'adjusting settings needed for panel weather...'
dconf write /org/mate/panel/objects/clock/prefs/cities "['<location name="" city="Αθήνα" timezone="Europe/Athens" latitude="37.933334" longitude="23.933332" code="LGAV" current="true"/>']"
dconf write /org/mate/panel/objects/clock/prefs/format "'24-hour'"
dconf write /org/mate/panel/objects/clock/prefs/speed-unit "'Beaufort scale'"
dconf write /org/mate/panel/objects/clock/prefs/show-weather "true"
dconf write /org/mate/panel/objects/clock/prefs/show-temperature "true"

# package cleanup
echo 'done, cleaning up...'
sudo mv official-package-repositories.list /etc/apt/sources.list.d/official-package-repositories.list
sudo apt-get update
sudo dpkg --purge libmarco-private0
sudo apt-get -f install -y
sudo apt-get update
sudo apt-get purge -y `deborphan`
sudo apt-get purge -y `deborphan`
sudo apt-get purge -y `deborphan`
sudo apt-get purge -y `deborphan`
sudo apt-get autoremove -y
sudo apt-get clean
echo 'basic installation, settings and cleaning up is done!'

echo 'you can also set your location in the mate panel to get weather updates (right click on the Clock -> Preferences)'
echo 'and you can also add an adblocker in your browser (both firefox and chromium) . Stin Priza suggests ublock origin plugin/addon.'

tput setaf 1; echo "thats all, please reboot now!"

tput sgr0

exit 0

