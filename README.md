# lmde2-mate-priza, scripts licensed under GPLv3

Customize LMDE2 Betsy MATE edition, for Stin Priza customers.
Be careful! Not to be used in general, always review before running!

Requirements: working network connection, dash.

Script to be run as regular user (not root!!!) :

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

$ wget https://framagit.org/stinpriza/lmde2-priza/raw/master/lmde2.sh

$ bash lmde2.sh

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You'll be asked for sudo pass (needed for system updates, etc).

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Extras script (install m$ fonts, skype) : 
$ wget https://framagit.org/stinpriza/lmde2-priza/raw/master/lmde2-extras.sh

$ bash lmde2-extras.sh

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

PS. LMDE2 is already too old and installation+upgrades are too many. 
LMDE3 (with Cinnamon), is also coming. 
